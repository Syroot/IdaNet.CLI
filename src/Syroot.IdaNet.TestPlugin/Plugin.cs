﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Syroot.IdaNet.Database;
using Syroot.IdaNet.Plugins;
using Syroot.IdaNet.Processor;

namespace Syroot.IdaNet.TestPlugin
{
    [Plugin(PluginAttribute.SdkVersion, Help = "Demonstrates IDA.NET capabilities.", Hotkey = "Alt-F11")]
    public class Plugin : IPlugin
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private IdaApplication _ida;
        private IdaDatabase _database;
        private IdaProcessor _processor;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Initialize()
        {
            _ida = IdaApplication.Current;

            _database = _ida.Database;
            _database.BytePatched += _database_BytePatched;

            _processor = _ida.Processor;
            _processor.NewFile += _processor_File;
            _processor.OldFile += _processor_File;
        }

        public void Terminate()
        {
            // TODO: Implement in PluginLoader.
        }

        // ---- EVENTHANDLER -------------------------------------------------------------------------------------------

        private void _database_BytePatched(object sender, BytePatchedEventArgs e)
        {
            KernelWin.MsgLine($"Value at 0x{e.Address:X8} was 0x{e.OriginalValue:X8}");
        }

        private void _processor_File(object sender, FileEventArgs e)
        {
            if (_database.FileType != FileType.Pe)
            {
                KernelWin.Error("Only PE executable file format is supported.");
                return;
            }

            foreach (StringInfo stringInfo in _ida.StringList)
            {

            }
        }
    }
}
