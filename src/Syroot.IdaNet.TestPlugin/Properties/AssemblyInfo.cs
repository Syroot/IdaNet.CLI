﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCopyright("(c) Syroot, licensed under MIT")]
[assembly: AssemblyDescription("Test plugin for Syroot IDA.NET")]
[assembly: AssemblyProduct("Syroot.IdaNet.TestPlugin")]
[assembly: AssemblyTitle("IDA.NET Test Plugin")]
[assembly: AssemblyVersion("0.1.0")]
[assembly: ComVisible(false)]
[assembly: Guid("09dfec08-8c15-4f62-8bf7-12736cdf1c3f")]
