#pragma once

namespace Syroot::IdaNet
{
	using namespace System;

	public ref class IdaEventArgs : EventArgs
	{
		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: property Int32 Result;
	};
}
