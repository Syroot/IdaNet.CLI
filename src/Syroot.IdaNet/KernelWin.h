#pragma once

#include <msclr/marshal.h>
#include "common.h"

namespace Syroot::IdaNet
{
	using namespace System;
	using namespace msclr::interop;

	public ref class KernelWin
	{
		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		private: KernelWin() { };

		// ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

		public: static void Error(String^ message);
		public: static void Error(String^ format, ... array<Object^>^ args);

		public: static Int32 Msg(String^ message);
		public: static Int32 Msg(String^ format, ... array<Object^>^ args);
		public: static Int32 MsgLine(String^ message);
		public: static Int32 MsgLine(String^ format, ... array<Object^>^ args);
	};
}
