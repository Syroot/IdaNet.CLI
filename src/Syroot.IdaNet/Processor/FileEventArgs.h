#pragma once

#include "..\IdaEventArgs.h"

namespace Syroot::IdaNet::Processor
{
	using namespace System;

	public ref class FileEventArgs : IdaEventArgs
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: String^ _fileName;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		public: FileEventArgs(String^ fileName)
			: _fileName(fileName) { }

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: property String^ FileName { String^ get() { return _fileName; } }
	};
}
