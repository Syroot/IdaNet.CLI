#include "IdaProcessor.h"

namespace Syroot::IdaNet::Processor
{
	// ---- METHODS (INTERNAL) -----------------------------------------------------------------------------------------

	int IdaProcessor::OnNewFile(String^ fileName)
	{
		FileEventArgs^ e = gcnew FileEventArgs(fileName);
		NewFile(this, e);
		return e->Result;
	}

	int IdaProcessor::OnOldFile(String^ fileName)
	{
		FileEventArgs^ e = gcnew FileEventArgs(fileName);
		OldFile(this, e);
		return e->Result;
	}
}
