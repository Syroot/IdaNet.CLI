#pragma once

#include "FileEventArgs.h"

namespace Syroot::IdaNet::Processor
{
	using namespace System;

	/// <summary>
	/// Represents the IDA processor module.
	/// </summary>
	public ref class IdaProcessor
	{
		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		internal: IdaProcessor() { }

		// ---- EVENTS -------------------------------------------------------------------------------------------------

		/// <summary>Occurs when a new file has been loaded.</summary>
		public: event EventHandler<FileEventArgs^>^ NewFile;
		/// <summary>Occurs when an old file has been loaded.</summary>
		public: event EventHandler<FileEventArgs^>^ OldFile;

		// ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

		internal: int OnNewFile(String^ fileName);
		internal: int OnOldFile(String^ fileName);
	};
}
