#pragma once

#include "common.h"
#include "IdaApplication.h"

namespace Syroot::IdaNet
{
	// ---- NATIVE -----------------------------------------------------------------------------------------------------

	int idaapi init(void);
	bool idaapi run(size_t);
	void idaapi term(void);
}
