#include "StringList.h"

namespace Syroot::IdaNet
{
	// ---- PROPERTIES -------------------------------------------------------------------------------------------------

	Int32 StringList::Count::get()
	{
		return (int)get_strlist_qty();
	}

	Boolean StringList::IsReadOnly::get()
	{
		return true;
	}

	// ---- OPERATORS --------------------------------------------------------------------------------------------------

	StringInfo StringList::default::get(int index)
	{
		string_info_t string_info;
		get_strlist_item(&string_info, index);
		return (StringInfo)Marshal::PtrToStructure(IntPtr(&string_info), StringInfo::typeid);
	}

	void StringList::default::set(int index, StringInfo value)
	{
		throw gcnew NotSupportedException();
	}

	// ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

	void StringList::Clear()
	{
		clear_strlist();
	}

	Boolean StringList::Contains(StringInfo item)
	{
		for each (StringInfo stringInfo in this)
		{
			if (stringInfo.Address == item.Address && stringInfo.Length == item.Length && stringInfo.Type == item.Type)
				return true;
		}
		return false;
	}

	void StringList::CopyTo(array<StringInfo>^ array, int arrayIndex)
	{
		for each (StringInfo stringInfo in this)
		{
			array[arrayIndex++] = stringInfo;
		}
	}

	System::Collections::Generic::IEnumerator<StringInfo>^ StringList::GetEnumerator()
	{
		return gcnew Enumerator(this);
	}

	Int32 StringList::IndexOf(StringInfo item)
	{
		for (int i = 0; i < Count; i++)
		{
			StringInfo stringInfo = this[i];
			if (stringInfo.Address == item.Address && stringInfo.Length == item.Length && stringInfo.Type == item.Type)
				return i;
		}
		return -1;
	}

	void StringList::Rebuild()
	{
		build_strlist();
	}

	// ---- METHODS (PROTECTED) ----------------------------------------------------------------------------------------

	void StringList::Add(StringInfo item)
	{
		throw gcnew NotSupportedException();
	}

	System::Collections::IEnumerator^ StringList::GetEnumeratorIEnumerable()
	{
		return (System::Collections::IEnumerator^)GetEnumerator();
	}

	void StringList::Insert(int index, StringInfo item)
	{
		throw gcnew NotSupportedException();
	}

	Boolean StringList::Remove(StringInfo item)
	{
		throw gcnew NotSupportedException();
	}

	void StringList::RemoveAt(int index)
	{
		throw gcnew NotSupportedException();
	}

	// ---- CLASSES, STRUCTS & ENUMS -----------------------------------------------------------------------------------

	// ---- Enumerator ----

	StringList::Enumerator::Enumerator(StringList^ list)
	{
		_list = list;
	}

	StringInfo StringList::Enumerator::Current::get()
	{
		return _list[_index++];
	}

	Object^ StringList::Enumerator::CurrentIEnumerator::get()
	{
		return Current;
	}

	bool StringList::Enumerator::MoveNext()
	{
		return ++_index < _list->Count;
	}

	void StringList::Enumerator::Reset()
	{
		_index = 0;
	}
}
