#include "PluginLoader.h"

namespace Syroot::IdaNet::Plugins
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	PluginLoader::PluginLoader()
	{
		// Attach the assembly resolve event to use the plugin assembly as the IDA.NET SDK assembly.
		_idaNetAssembly = Assembly::GetExecutingAssembly();
		_idaNetAssemblyName = _idaNetAssembly->GetName();
		AppDomain::CurrentDomain->AssemblyResolve += gcnew ResolveEventHandler(this, &PluginLoader::AppDomain_AssemblyResolve);

		// Load plugins from the IDA plugin directories (including the possibly set IDAUSR environment variable).
		Plugins = gcnew List<Plugin^>();

		WCHAR idaExe[MAX_PATH];
		GetModuleFileNameW(NULL, idaExe, MAX_PATH);
		ScanPluginDirectory(Path::Combine(Path::GetDirectoryName(marshal_as<String^>(idaExe)), "plugins"));

		String^ idaUsr = Environment::GetEnvironmentVariable("IDAUSR");
		if (!String::IsNullOrEmpty(idaUsr))
			ScanPluginDirectory(Path::Combine(idaUsr, "plugins"));

		// DEBUG: Simply call Initialize on them for now.
		for each (Plugin^ plugin in Plugins)
		{
			plugin->Instance->Initialize();
		}
	}

	// ---- METHODS (INTERNAL) -----------------------------------------------------------------------------------------

	void PluginLoader::Terminate()
	{
		// DEBUG: Simply call Terminate on them for now.
		for each (Plugin^ plugin in Plugins)
		{
			plugin->Instance->Terminate();
		}
	}

	// ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

	void PluginLoader::ScanPluginDirectory(String^ pluginDirectory)
	{
		String^ directory = Path::Combine(pluginDirectory, _pluginSubDirectory);
		KernelWin::Msg("Checking directory \"{0}\"... ", directory);

		// Load plugins from the dotnet subfolder in this IDA plugin directory.
		if (!Directory::Exists(directory))
		{
			KernelWin::MsgLine("not found");
			return;
		}
		else
		{
			KernelWin::MsgLine("ok");
		}

		// Load all assemblies found in the directory.
		for each (String^ fileName in Directory::GetFiles(directory, "*.dll"))
		{
			Plugin^ plugin = LoadPlugin(fileName);
			if (plugin)
				Plugins->Add(plugin);
		}
	}

	Plugin^ PluginLoader::LoadPlugin(String^ fileName)
	{
		KernelWin::Msg("Checking assembly \"{0}\"... ", Path::GetFileName(fileName));
		Plugin^ plugin = nullptr;

		try
		{
			// Ignore the IDA.NET SDK assembly itself if someone accidentally put it there.
			if (Path::GetFileName(fileName) == Path::GetFileName(_idaNetAssembly->Location))
				throw gcnew InvalidOperationException("IDA.NET SDK assembly may not be put into dotnet plugin folder");

			// Try to load the assembly and iterate through all types.
			Assembly^ assembly = Assembly::LoadFile(fileName);
			for each (Type^ type in assembly->ExportedTypes)
			{
				// Find an IIdaPlugin implementation.
				if (!IPlugin::typeid->IsAssignableFrom(type))
					continue;

				// Get an IdaPluginAttribute safely.
				array<Object^>^ attributes = type->GetCustomAttributes(PluginAttribute::typeid, false);
				if (attributes->Length != 1)
					continue;
				PluginAttribute^ attribute = (PluginAttribute^)attributes[0];
				if (attribute->SupportedVersion != IDP_INTERFACE_VERSION)
					throw gcnew InvalidOperationException(String::Format("wrong IDA SDK version {0}, expected {1}",
						attribute->SupportedVersion, IDP_INTERFACE_VERSION));

				// Instantiate the plugin and store the instance and attribute information.
				IPlugin^ idaPlugin = (IPlugin^)Activator::CreateInstance(type, false);
				plugin = gcnew Plugin(assembly, idaPlugin, attribute);
				break;
			}
			if (plugin)
				KernelWin::MsgLine("loaded \"{0}\"", plugin->Title);
			else
				KernelWin::MsgLine("not an IDA.NET plugin");
		}
		catch (Exception^ ex) 
		{
			// Either not a valid / managed assembly, bad attribute, or bad implementation.
			KernelWin::MsgLine("not loaded ({0})", ex->Message);
		}

		return plugin;
	}

	// ---- EVENTHANDLERS ----------------------------------------------------------------------------------------------

	Assembly^ PluginLoader::AppDomain_AssemblyResolve(Object^ sender, ResolveEventArgs^ e)
	{
		AssemblyName^ assemblyName = gcnew AssemblyName(e->Name);

		// If this is the IDA.NET SDK assembly, return the running instance.
		if (assemblyName->Name == _idaNetAssemblyName->Name)
			return _idaNetAssembly;

		// TODO: Otherwise search for the corresponding assembly in the .NET plugins folder.
		return nullptr;
	}
}
