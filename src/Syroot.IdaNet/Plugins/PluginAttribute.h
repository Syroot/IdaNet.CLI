#pragma once

#include "../common.h"
#include "PluginFlags.h"

namespace Syroot::IdaNet::Plugins
{
	using namespace System;

	[AttributeUsage(AttributeTargets::Class)]
	public ref class PluginAttribute : public Attribute
	{
		// ---- CONSTANTS ----------------------------------------------------------------------------------------------

		/// <summary>
		/// The supported SDK version plugins using this assembly are compiled against.
		/// </summary>
		public: literal UInt32 SdkVersion = IDP_INTERFACE_VERSION;

		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: UInt32 _supportedVersion;
		private: PluginFlags _pluginFlags;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		/// <summary>
		/// Initializes a new instance of the <see cref="PluginAttribute"/> class compiled against the given
		/// <paramref name="supportedVersion"/> of the SDK.
		/// </summary>
		/// <param name="supportedVersion">The supported SDK version.</param>
		public: PluginAttribute(UInt32 supportedVersion)
			: _supportedVersion(supportedVersion), _pluginFlags(PluginFlags::None) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="PluginAttribute"/> class compiled against the given
		/// <paramref name="supportedVersion"/> of the SDK and with the given <paramref name="pluginFlags"/>.
		/// </summary>
		/// <param name="supportedVersion">The supported SDK version.</param>
		/// <param name="pluginFlags">The <see cref="PluginFlags"/> controlling plugin lifecycle behavior.</param>
		public: PluginAttribute(UInt32 supportedVersion, PluginFlags pluginFlags)
			: _supportedVersion(supportedVersion), _pluginFlags(pluginFlags) { }

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		/// <summary>
		/// Gets the SDK version the plugin is compiled against. If this is not equal to <see cref="SdkVersion"/>, the
		/// plugin cannot be loaded.
		/// </summary>
		public: property UInt32 SupportedVersion
		{
			public: UInt32 get() { return _supportedVersion; }
		}

		/// <summary>
		/// Gets the <see cref="PluginFlags"/> controlling the plugin lifecycle behavior.
		/// </summary>
		public: property PluginFlags Flags
		{
			public: PluginFlags get() { return _pluginFlags; }
		}

		public: property String^ Help;

		public: property String^ Hotkey;
	};
}
