#pragma once

#include "IPlugin.h"
#include "PluginAttribute.h"

namespace Syroot::IdaNet::Plugins
{
	using namespace System;
	using namespace System::Reflection;

	public ref class Plugin
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: Assembly^ _assembly;
		private: IPlugin^ _instance;
		private: PluginFlags _flags;
		private: String^ _title;
		private: String^ _description;
		private: String^ _help;
		private: String^ _hotkey;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		internal: Plugin(Assembly^ assembly, IPlugin^ instance, PluginAttribute^ attribute);

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: property Assembly^ assembly
		{
			public: Assembly^ get() { return _assembly; }
		}

		public: property IPlugin^ Instance
		{
			public: IPlugin^ get() { return _instance; }
		}

		public: property PluginFlags Flags
		{
			public: PluginFlags get() { return _flags; }
		}

		public: property String^ Title
		{
			public: String^ get() { return _title; }
		}

		public: property String^ Description
		{
			public: String^ get() { return _description; }
		}

		public: property String^ Help
		{
			public: String^ get() { return _help; }
		}

		public: property String^ Hotkey
		{
			public: String^ get() { return _hotkey; }
		}

		// ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

		private: generic<typename T> where T : Attribute
			T GetAttribute();
	};
}
