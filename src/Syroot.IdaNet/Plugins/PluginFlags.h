#pragma once

namespace Syroot::IdaNet::Plugins
{
	using namespace System;

	[Flags]
	public enum class PluginFlags : UInt32
	{
		None = 0,
		Modify = 1 << 0,
		Draw = 1 << 1,
		Segment = 1 << 2,
		Unload = 1 << 3,
		Hide = 1 << 4,
		Debugger = 1 << 5,
		Processor = 1 << 6,
		Fixed = 1 << 7
	};
}
