#pragma once

#include <msclr/marshal.h>
#include "../common.h"
#include "../KernelWin.h"
#include "IPlugin.h"
#include "Plugin.h"
#include "PluginAttribute.h"

namespace Syroot::IdaNet::Plugins
{
	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::IO;
	using namespace System::Reflection;
	using namespace msclr::interop;

	public ref class PluginLoader
	{
		// ---- CONSTANTS ----------------------------------------------------------------------------------------------

		private: literal String^ _pluginSubDirectory = "dotnet";

		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: Assembly^ _idaNetAssembly;
		private: AssemblyName^ _idaNetAssemblyName;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		internal: PluginLoader();

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: property IList<Plugin^>^ Plugins;
			
		// ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

		internal: void Terminate();

		// ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

		private: void ScanPluginDirectory(String^ pluginDirectory);
		private: Plugin^ LoadPlugin(String^ assemblyFileName);

		// ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

		private: Assembly^ AppDomain_AssemblyResolve(Object^ sender, ResolveEventArgs^ e);
	};
}
