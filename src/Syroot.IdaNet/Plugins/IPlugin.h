#pragma once

namespace Syroot::IdaNet::Plugins
{
	public interface class IPlugin
	{
		// ---- METHODS ------------------------------------------------------------------------------------------------

		void Initialize();

		void Terminate();
	};
}
