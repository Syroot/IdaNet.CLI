#include "Plugin.h"

namespace Syroot::IdaNet::Plugins
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	Plugin::Plugin(Assembly^ assembly, IPlugin^ instance, PluginAttribute^ attribute)
	{
		_assembly = assembly;
		_instance = instance;
		_flags = attribute->Flags;
		_title = GetAttribute<AssemblyTitleAttribute^>()->Title;
		_description = GetAttribute<AssemblyDescriptionAttribute^>()->Description;
		_help = attribute->Help;
		_hotkey = attribute->Hotkey;
	}

	generic<typename T> where T : Attribute
	T Plugin::GetAttribute()
	{
		array<Object^>^ attributes = _assembly->GetCustomAttributes(T::typeid, false);
		return attributes->Length == 1 ? (T)attributes[0] : T();
	}
}

