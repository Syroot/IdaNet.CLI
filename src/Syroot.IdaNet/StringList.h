#pragma once

#include "common.h"
#include "StringInfo.h"

namespace Syroot::IdaNet
{
	using namespace System;
	using namespace System::Collections::Generic;

	/// <summary>
	/// Represents the string list of the IDA database.
	/// </summary>
	public ref class StringList : IList<StringInfo>, IReadOnlyCollection<StringInfo>
	{
		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		internal: StringList() { }

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: virtual property Int32 Count { Int32 get(); }

		public: virtual property Boolean IsReadOnly { Boolean get(); }

		// ---- OPERATORS ----------------------------------------------------------------------------------------------

		public: virtual property StringInfo default[int]
		{
			StringInfo get(int index);
			void set(int index, StringInfo value);
		}

		// ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

		public: virtual void Clear() = IList<StringInfo>::Clear;
		public: virtual Boolean Contains(StringInfo item);
		public: virtual void CopyTo(array<StringInfo>^ array, int arrayIndex);
		public: virtual System::Collections::Generic::IEnumerator<StringInfo>^ GetEnumerator();
		public: virtual Int32 IndexOf(StringInfo item);
		public: void Rebuild();

		// ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

		protected: virtual void Add(StringInfo item) = IList<StringInfo>::Add;
		protected: virtual void Insert(int index, StringInfo item) = IList<StringInfo>::Insert;
		protected: virtual System::Collections::IEnumerator^ GetEnumeratorIEnumerable() = System::Collections::IEnumerable::GetEnumerator;
		protected: virtual Boolean Remove(StringInfo item) = IList<StringInfo>::Remove;
		protected: virtual void RemoveAt(int index) = IList<StringInfo>::RemoveAt;

		// ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

		ref struct Enumerator : IEnumerator<StringInfo>
		{
			private: StringList^ _list;
			private: Int32 _index;

			internal: Enumerator(StringList^ list);

			~Enumerator() { }

			public: virtual property StringInfo Current
			{
				StringInfo get();
			}

			protected: virtual property Object^ CurrentIEnumerator
			{
				Object^ get() = System::Collections::IEnumerator::Current::get;
			}

			public: virtual bool MoveNext();

			public: virtual void Reset();
		};
	};
}
