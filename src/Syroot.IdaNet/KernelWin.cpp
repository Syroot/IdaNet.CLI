#include "KernelWin.h"

namespace Syroot::IdaNet
{
	// ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

	void KernelWin::Error(String^ message)
	{
		marshal_context context;
		return error(context.marshal_as<const char*>(message));
	};

	void KernelWin::Error(String^ format, ... array<Object^>^ args)
	{
		return KernelWin::Error(String::Format(format, args));
	}

	Int32 KernelWin::Msg(String^ message)
	{
		marshal_context context;
		return msg(context.marshal_as<const char*>(message));
	};

	Int32 KernelWin::Msg(String^ format, ... array<Object^>^ args)
	{
		return KernelWin::Msg(String::Format(format, args));
	}

	Int32 KernelWin::MsgLine(String^ message)
	{
		return KernelWin::Msg(message + "\n");
	}

	Int32 KernelWin::MsgLine(String^ message, ... array<Object^>^ args)
	{
		return KernelWin::Msg(String::Format(message, args) + "\n");
	}
}
