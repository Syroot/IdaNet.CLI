#pragma once

namespace Syroot::IdaNet::Database
{
	using namespace System;

	[Flags]
	public enum class AnalysisFlags : UInt32
	{
		None = 0,
		Code = 1 << 0,
		MarkCode = 1 << 1,
		JumpTables = 1 << 2,
		PureData = 1 << 3,
		Used = 1 << 4,
		Unknown = 1 << 5,
		ProcedurePointers = 1 << 6,
		Procedures = 1 << 7,
		FunctionTails = 1 << 8,
		LVars = 1 << 9,
		StackArguments = 1 << 10,
		RegisterArguments = 1 << 11,
		Trace = 1 << 12,
		VerifyStackPointer = 1 << 13,
		NoReturn = 1 << 14,
		MemberFunctions = 1 << 15,
		TruncateFunctions = 1 << 16,
		StringLiterals = 1 << 17,
		CheckUnicode = 1 << 18,
		Fixup = 1 << 19,
		DereferenceOffsets = 1 << 20,
		ImmOffsets = 1 << 21,
		DataOffsets = 1 << 22,
		Flirt = 1 << 23,
		SignatureComments = 1 << 24,
		SignatureMultiplication = 1 << 25,
		HideFlirt = 1 << 26,
		JumpFunctions = 1 << 27,
		NullSubs = 1 << 28,
		DoData = 1 << 29,
		DoCode = 1 << 30,
		Final = 1u << 31
	};

	[Flags]
	public enum class AnalysisFlags2 : UInt32
	{
		None = 0,
		DoEH = 1 << 0,
		DoRtti = 1 << 1
	};
}
