#pragma once

namespace Syroot::IdaNet::Database
{
	using namespace System;

	[Flags]
	public enum class DatabaseFlags : UInt32
	{
		None = 0,
		Auto = 1 << 0,
		AllAssembler = 1 << 1,
		LoadIdc = 1 << 2,
		NoUser = 1 << 3,
		ReadOnly = 1 << 4,
		CheckOperands = 1 << 5,
		NonMatchedOperands = 1 << 6,
		GraphView = 1 << 7
	};
}
