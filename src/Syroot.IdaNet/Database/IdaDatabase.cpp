#include "IdaDatabase.h"

namespace Syroot::IdaNet::Database
{
	// ---- PROPERTIES -------------------------------------------------------------------------------------------------

	// ---- IdaInfo ----

	String^ IdaDatabase::Tag::get()
	{
		return marshal_as<String^>(inf.tag);
	}

	UInt16 IdaDatabase::Version::get()
	{
		return inf.version;
	}

	String^ IdaDatabase::ProcessorName::get()
	{
		return marshal_as<String^>(inf.procname);
	}

	DatabaseFlags IdaDatabase::Flags::get()
	{
		return (DatabaseFlags)inf.s_genflags;
	}

	LoaderFlags IdaDatabase::LoaderFlags::get()
	{
		return (Syroot::IdaNet::Database::LoaderFlags)inf.lflags;
	}

	UInt32 IdaDatabase::ChangeCount::get()
	{
		return inf.database_change_count;
	}

	FileType IdaDatabase::FileType::get()
	{
		return (Syroot::IdaNet::Database::FileType)inf.filetype;
	}

	UInt16 IdaDatabase::OSType::get()
	{
		return inf.ostype;
	}

	UInt16 IdaDatabase::AppType::get()
	{
		return inf.apptype;
	}

	Byte IdaDatabase::AssemblerType::get()
	{
		return inf.asmtype;
	}

	SpecialSegmentFormat IdaDatabase::SpecialSegmentFormat::get()
	{
		return (Syroot::IdaNet::Database::SpecialSegmentFormat)inf.specsegs;
	}

	AnalysisFlags IdaDatabase::AnalysisFlags::get()
	{
		return (Syroot::IdaNet::Database::AnalysisFlags)inf.af;
	}

	AnalysisFlags2 IdaDatabase::AnalysisFlags2::get()
	{
		return (Syroot::IdaNet::Database::AnalysisFlags2)inf.af2;
	}

	// ---- METHODS (INTERNAL) -----------------------------------------------------------------------------------------

	int IdaDatabase::OnKernelConfigLoaded()
	{
		IdaEventArgs^ e = gcnew IdaEventArgs();
		KernelConfigLoaded(this, e);
		return e->Result;
	}

	int IdaDatabase::OnBytePatched(ADDRESS address, UInt32 originalValue)
	{
		BytePatchedEventArgs^ e = gcnew BytePatchedEventArgs(address, originalValue);
		BytePatched(this, e);
		return e->Result;
	}
}
