#pragma once

namespace Syroot::IdaNet::Database
{
	using namespace System;

	public enum class SpecialSegmentFormat : Byte
	{
		None = 0,
		Use4Bytes = 4,
		Use8Bytes = 8
	};
}
