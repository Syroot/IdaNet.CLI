#pragma once

#include "..\common.h"
#include "..\IdaEventArgs.h"

namespace Syroot::IdaNet::Database
{
	using namespace System;

	public ref class BytePatchedEventArgs : IdaEventArgs
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: ADDRESS _address;
		private: UInt32 _originalValue;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		public: BytePatchedEventArgs(ADDRESS address, UInt32 originalValue)
			: _address(address), _originalValue(originalValue) { }

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		public: property ADDRESS Address { ADDRESS get() { return _address; } }
		public: property UInt32 OriginalValue { UInt32 get() { return _originalValue; } }
	};
}
