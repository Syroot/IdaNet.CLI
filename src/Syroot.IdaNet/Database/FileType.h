#pragma once

namespace Syroot::IdaNet::Database
{
	using namespace System;

	public enum class FileType : UInt16
	{
		ExeOld,
		ComOld,
		Bin,
		Drv,
		Win,
		Hex,
		Mex,
		Lx,
		Le,
		Nlm,
		Coff,
		Pe,
		Omf,
		SRec,
		Zip,
		OmfLib,
		Ar,
		Loader,
		Elf,
		W32Run,
		AOut,
		Prc,
		Exe,
		Com,
		Aix,
		MacHo,
	};
}
