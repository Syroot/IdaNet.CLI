#pragma once

namespace Syroot::IdaNet::Database
{
	using namespace System;

	[Flags]
	public enum class LoaderFlags : UInt32
	{
		None = 0,
		DecodeFpp = 1 << 0,
		Is32BIt = 1 << 1,
		Is64Bit = 1 << 2,
		IsDll = 1 << 3,
		FlatOffset32 = 1 << 4,
		BigEndian = 1 << 5,
		WideHighByteFirst = 1 << 6,
		NoDebuggerPath = 1 << 7,
		Snapshot = 1 << 8,
		Pack = 1 << 9,
		Compress = 1 << 10,
		KernelMode = 1 << 11
	};
}
