#pragma once

#include <msclr/marshal.h>
#include "../common.h"
#include "../IdaEventArgs.h"
#include "AnalysisFlags.h"
#include "BytePatchedEventArgs.h"
#include "DatabaseFlags.h"
#include "FileType.h"
#include "LoaderFlags.h"
#include "SpecialSegmentFormat.h"

namespace Syroot::IdaNet::Database
{
	using namespace System;
	using namespace msclr::interop;

	/// <summary>
	/// Represents the IDA database.
	/// </summary>
	public ref class IdaDatabase
	{
		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		internal: IdaDatabase() { }

		// ---- EVENTS -------------------------------------------------------------------------------------------------

		/// <summary>Occurs when a byte has been patched.</summary>
		public: event EventHandler<BytePatchedEventArgs^>^ BytePatched;
		/// <summary>Occurs when the ida.cfg is parsed.</summary>
		public: event EventHandler<IdaEventArgs^>^ KernelConfigLoaded;

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		// ---- IdaInfo ----

		/// <summary>Gets the IDA info structure tag.</summary>
		public: property String^ Tag { String^ get(); }
		/// <summary>Gets the version of the database.</summary>
		public: property UInt16 Version { UInt16 get(); }
		/// <summary>Gets the name of the current processor.</summary>
		public: property String^ ProcessorName { String^ get(); }
		/// <summary>Gets the general IDA info structure flags.</summary>
		public: property DatabaseFlags Flags { DatabaseFlags get(); }
		/// <summary>Gets the miscellaneous database flags.</summary>
		public: property LoaderFlags LoaderFlags { Syroot::IdaNet::Database::LoaderFlags get(); }
		/// <summary>Gets the value of a counter incremented after each byte and regular segment modifications.</summary>
		public: property UInt32 ChangeCount { UInt32 get(); }
		/// <summary>Gets the input file type.</summary>
		public: property FileType FileType { Syroot::IdaNet::Database::FileType get(); }
		/// <summary>Gets the OS type of the program.</summary>
		public: property UInt16 OSType { UInt16 get(); }
		/// <summary>Gets the application type.</summary>
		public: property UInt16 AppType { UInt16 get(); }
		/// <summary>Gets the target assembler number.</summary>
		public: property Byte AssemblerType { Byte get(); }
		/// <summary>Gets the special segment format to use.</summary>
		public: property SpecialSegmentFormat SpecialSegmentFormat { Syroot::IdaNet::Database::SpecialSegmentFormat get(); }
		/// <summary>Gets the first set of analysis flags.</summary>
		public: property AnalysisFlags AnalysisFlags { Syroot::IdaNet::Database::AnalysisFlags get(); }
		/// <summary>Gets the second set of analysis flags.</summary>
		public: property AnalysisFlags2 AnalysisFlags2 { Syroot::IdaNet::Database::AnalysisFlags2 get(); }

		// ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

		internal: int OnBytePatched(ADDRESS address, UInt32 originalValue);
		internal: int OnKernelConfigLoaded();
	};
}
