using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

[assembly:AssemblyCopyright("(c) Syroot, licensed under MIT")];
[assembly:AssemblyCompany("Syroot")];
[assembly:AssemblyDescription("IDA.NET Plugin Library")];
[assembly:AssemblyTitle("Syroot.IdaNet")];
[assembly:AssemblyVersion("0.1.0")];
[assembly:CLSCompliant(true)];
[assembly:ComVisible(false)];
[assembly:Guid("838ed65f-9c79-4786-be14-0d5827e9bb2f")];
