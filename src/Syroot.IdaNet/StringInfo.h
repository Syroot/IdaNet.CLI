#pragma once

#include "common.h"

namespace Syroot::IdaNet
{
	using namespace System;
	using namespace System::Runtime::InteropServices;

	[StructLayout(LayoutKind::Sequential)]
	public value struct StringInfo
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		public: ADDRESS Address;
		public: Int32 Length;
		public: Int32 Type;
	};
}
