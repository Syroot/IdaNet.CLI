#include "IdaApplication.h"

namespace Syroot::IdaNet
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	IdaApplication::IdaApplication()
	{
		if (_current)
			throw gcnew InvalidOperationException("IdaApplication has already been initialized.");
		_current = this;

		_database = gcnew IdaDatabase();
		_processor = gcnew IdaProcessor();
		_stringList = gcnew Syroot::IdaNet::StringList();
		_pluginLoader = gcnew Syroot::IdaNet::Plugins::PluginLoader();

		// Hook up IDA events.
		hook_to_notification_point(HT_IDB, idb_callback, NULL);
		hook_to_notification_point(HT_IDP, idp_callback, NULL);
	}

	IdaApplication::~IdaApplication()
	{
		_current = nullptr;

		this->!IdaApplication();
	}

	IdaApplication::!IdaApplication()
	{
		// Unhook from IDA events.
		unhook_from_notification_point(HT_IDB, idb_callback, NULL);
		unhook_from_notification_point(HT_IDP, idp_callback, NULL);
	}

	// ---- NATIVE -----------------------------------------------------------------------------------------------------

	// ---- IDB Events ----

#pragma managed(push, off)
	ssize_t idaapi idb_callback(void* user_data, int notification_code, va_list va)
	{
		switch (notification_code)
		{
			case idb_event::byte_patched:
			{
				ea_t ea = va_arg(va, ea_t);
				uint32 old_value = va_arg(va, uint32);
				return idb_byte_patched(ea, old_value);
			}
			case idb_event::kernel_config_loaded:
			{
				return idb_generic(notification_code);
			}
		}
		return 0;
	}
#pragma managed(pop)

	ssize_t idb_byte_patched(ea_t ea, uint32 old_value)
	{
		return IdaApplication::Current->Database->OnBytePatched(ea, old_value);
	}

	ssize_t idb_generic(int notification_code)
	{
		switch (notification_code)
		{
			case idb_event::kernel_config_loaded:
				return IdaApplication::Current->Database->OnKernelConfigLoaded();
		}
		return 0;
	}

	// ---- IDP Events ----

#pragma managed(push, off)
	ssize_t idaapi idp_callback(void* user_data, int notification_code, va_list va)
	{
		switch (notification_code)
		{
			case processor_t::event_t::ev_newfile:
			{
				char* fname = va_arg(va, char*);
				return idp_newfile(fname);
			}
			case processor_t::event_t::ev_oldfile:
			{
				char* fname = va_arg(va, char*);
				return idp_oldfile(fname);
			}
		}
		return 0;
	}
#pragma managed(pop)

	ssize_t idp_newfile(char* fname)
	{
		String^ fileName = marshal_as<String^>(fname);
		return IdaApplication::Current->Processor->OnNewFile(fileName);
	}

	ssize_t idp_oldfile(char* fname)
	{
		String^ fileName = marshal_as<String^>(fname);
		return IdaApplication::Current->Processor->OnOldFile(fileName);
	}

	ssize_t idp_generic(int notification_code)
	{
		return 0;
	}
}
