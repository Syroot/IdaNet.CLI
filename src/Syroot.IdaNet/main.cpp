#include "main.h"

namespace Syroot::IdaNet
{
	// ---- NATIVE -----------------------------------------------------------------------------------------------------

	int idaapi init(void)
	{
		String^ separator = gcnew String('-', 60);
		KernelWin::MsgLine(separator);
		KernelWin::MsgLine("IDA.NET initializing...");

		bool initialized;
		try
		{
			IdaApplication^ idaApplication = gcnew IdaApplication();
			initialized = true;
			Int32 pluginCount = idaApplication->PluginLoader->Plugins->Count;
			KernelWin::MsgLine("IDA.NET initialized with {0} plugin{1}.",
				pluginCount, pluginCount == 1 ? String::Empty : "s");
		}
		catch (Exception^ ex)
		{
			KernelWin::MsgLine("IDA.NET could not be initialized: " + ex->Message);
		}

		KernelWin::MsgLine(separator);
		return initialized ? PLUGIN_KEEP : PLUGIN_SKIP;
	}

	bool idaapi run(size_t)
	{
		return true;
	}

	void idaapi term(void)
	{
		delete IdaApplication::Current;
	}
}

plugin_t PLUGIN =
{
	IDP_INTERFACE_VERSION, PLUGIN_FIX,
	Syroot::IdaNet::init, Syroot::IdaNet::term, Syroot::IdaNet::run,
	"Syroot IDA.NET", // comment
	"Loads and manages .NET assemblies from the plugins directory.", // help
	"IDA.NET", // name
	"Alt-F12" // hotkey
};
