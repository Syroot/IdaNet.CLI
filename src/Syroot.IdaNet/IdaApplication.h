#pragma once

#include <msclr/marshal.h>
#include "StringList.h"
#include "Database/IdaDatabase.h"
#include "Plugins/PluginLoader.h"
#include "Processor/IdaProcessor.h"

namespace Syroot::IdaNet
{
	using namespace System;
	using namespace Syroot::IdaNet::Database;
	using namespace Syroot::IdaNet::Plugins;
	using namespace Syroot::IdaNet::Processor;

	/// <summary>
	/// Represents the running IDA application and provides access to its components, and is thus a singleton class.
	/// </summary>
	public ref class IdaApplication
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: static IdaApplication^ _current;

		private: IdaDatabase^ _database;
		private: PluginLoader^ _pluginLoader;
		private: IdaProcessor^ _processor;
		private: StringList^ _stringList;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------
		
		internal: IdaApplication();

		private: ~IdaApplication();

		protected: !IdaApplication();

		// ---- PROPERTIES ---------------------------------------------------------------------------------------------

		/// <summary>
		/// Gets the singleton instance of the running <see cref="IdaApplication"/>.
		/// </summary>
		public: static property IdaApplication^ Current { IdaApplication^ get() { return _current; } }

		/// <summary>
		/// Gets the <see cref="IdaDatabase"/> instance providing access to the loaded database.
		/// </summary>
		public: property IdaDatabase^ Database { IdaDatabase^ get() { return _database; } }

		/// <summary>
		/// Gets the IDA.NET <see cref="PluginLoader"/> instance providing access to managed plugins.
		/// </summary>
		public: property PluginLoader^ PluginLoader { Syroot::IdaNet::Plugins::PluginLoader^ get() { return _pluginLoader; } }

		/// <summary>
		/// Gets the <see cref="IdaProcessor"/> instance providing access to the processor module.
		/// </summary>
		public: property IdaProcessor^ Processor { IdaProcessor^ get() { return _processor; } }

		/// <summary>
		/// Gets the <see cref="StringList"/> storing the database string information.
		/// </summary>
		public: property StringList^ StringList { Syroot::IdaNet::StringList^ get() { return _stringList; } }
	};

	// ---- NATIVE -----------------------------------------------------------------------------------------------------

	// ---- IDB Events ----

	ssize_t idaapi idb_callback(void* user_data, int notification_code, va_list va);
	ssize_t idb_byte_patched(ea_t address, uint32 old_value);
	ssize_t idb_generic(int notification_code);

	// ---- IDP Events ----

	ssize_t idaapi idp_callback(void* user_data, int notification_code, va_list va);
	ssize_t idp_newfile(char* fname);
	ssize_t idp_oldfile(char* fname);
	ssize_t idp_generic(int notification_code);
}
