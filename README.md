# IDA.NET (C++/CLI)

This project was an experiment to load .NET assemblies into IDA 7.0 via C++/CLI. It is now obsoleted by the new .NET Core 2.0-based [IdaNet](https://github.com/Syroot/IdaNet) project since C++/CLI has no longterm support plans. The original contents of the readme follow.

## How does it work?

- Since IDA scans for native C++ assemblies in the `plugins` directory, the main plugin is a C++/CLI DLL which exports
the native `PLUGIN` class and implements the IDA lifecycle methods `init`, `term` and `run`.
- Inside of `init` it then initializes a managed `PluginLoader` class looking for .NET assemblies in the
`plugins/dotnet` folder (if it exists).
- A valid .NET plugin has one class implementing the `Syroot.IdaNet.Plugins.IPlugin` interface which provides
`Initialize` and `Terminate` methods similar to the native IDA methods (plugin flags are not supported at this moment
and `Initialize` is immediately called at program start).
- The class implementing `IPlugin` also has to be decorated with the `PluginAttribute`, providing the IDA SDK version it
is compiled against, and plugin information for `Hotkey` or `Help` - other information is taken
from assembly attributes (comment maps to `AssemblyDescription` and name to `AssemblyTitle`).
- In the .NET plugin, the IDA SDK can be accessed through SDK wrappers _also_ provided by the main plugin DLL - to start
writing .NET IDA code, add a reference to the C++/CLI plugin assembly.
  - The plugin assembly does not need to be copied to the `plugins/dotnet` folder, as the `PluginLoader` automatically
resolves references to it by reusing the main plugin DLL.

## Installation

- Download the source code and open the main solution.
- Open `PropertySheet.props` and provide the following paths at the top `PropertyGroup`:
  - **IDADIR**: The directory you have installed IDA to. Used to start debugging through `ida(64).exe`.
  - **IDAPLUGINS**: The native plugin directory you want to copy the resulting IDA.NET plugin to. May be outside of
  `IDADIR` if you use the `IDAUSR` environment variable.
  - **IDASDK**: The native IDA SDK directory (`idasdk70`).
- Build `Syroot.IdaNet`.
- Create a `plugins/dotnet` subfolder.

## Creating a plugin

- Create a .NET assembly and reference the `Syroot.IdaNet` assembly.
- Import the `Syroot.IdaNet.Plugins` namespace.
- Add a class implementing `IPlugin` and decorate it with the `PluginAttribute`.
- Write code accessing the IDA SDK through the wrapper library provided in the `Syroot.IdaNet` namespace. Your starting
point is the `IdaApplication.Current` instance, representing the currently running IDA application and allowing access
to any subsystems like the `Database`.
- Place the resulting .NET assembly in the `plugins/dotnet` subfolder.

## Support

You can ask questions and suggest features on Discord aswell. Feel free to [join the Syroot server](https://discord.gg/42M6J5v)!
